$(document).ready(function() {

	(function() {
		var mySlidebars = new $.slidebars();
		$('#btn-menu').on('click', function() {
			mySlidebars.slidebars.toggle('right');
		});
	})();

	$('[data-toggle]').on('click', function(){
		var toggle =  $(this).attr('data-toggle');

		if (toggle){
			$(toggle).slideToggle(100);
		}
	});

	$('.scale-list').on('click', 'label', function(){
		var scaleList = $(this).closest('.scale-list');
		var active = $(this).closest('li').index();

		if (active > 0){
			scaleList.find('label').each(function(){
				$(this).removeClass('active');

				if ($(this).closest('li').index() < active){
					$(this).addClass('active');
				}
			})

		}
	});

	(function(){
		$animateIn = $(".animate-in");
		var animateInOffset = 50;

		if (browserSupportsCSSProperty('animation') && browserSupportsCSSProperty('transition')) {
			$animateIn.addClass("pre-animate");
		}

		$(window).scroll(function(e) {
			var windowHeight = $(window).height(),
			windowScrollPosition = $(window).scrollTop(),
			bottomScrollPosition = windowHeight + windowScrollPosition;

			$animateIn.each(function(i, element) {
				if ($(element).offset().top + animateInOffset < bottomScrollPosition) {
					$(element).removeClass('pre-animate');
				}
			});
		});

		function browserSupportsCSSProperty(propertyName) {
		  var elm = document.createElement('div');
		  propertyName = propertyName.toLowerCase();

		  if (elm.style[propertyName] != undefined)
		    return true;

		  var propertyNameCapital = propertyName.charAt(0).toUpperCase() + propertyName.substr(1),
		    domPrefixes = 'Webkit Moz ms O'.split(' ');

		  for (var i = 0; i < domPrefixes.length; i++) {
		    if (elm.style[domPrefixes[i] + propertyNameCapital] != undefined)
		      return true;
		  }

		  return false;
		}
		
	})();
});
